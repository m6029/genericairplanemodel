# Generic Airplane Model (GAM) V2.0

Generic Airplane Model (GAM) is a Python toolbox for **preliminary airplane design based on statistical regressions**. GAM covers various airplane propulsion systems, including conventional thermal engines, electric engines, and fuel cells, as well as various energy sources such as kerosene, hydrogen, batteries, methane, and ammonia.

The tool was developed by ENAC and ISAE-SUPAERO in Toulouse, France.

***

## Principle

The airplane design procedure usually involves Multidisciplanary Design Optimization (MDO), with strong interdisciplinary couplings.
The Generic Airplane Models is an extremely simplified procedure which relies on empirical regressions. It can be used to quicly estimate the mass and performances
of a tube-and-wing airplane.

See below an example of correlation between Operational Weight Empty (OWE) and Maximum Take-Off Weight (MTOW).

![OWE versus MTOW](img/owe_vs_mtow.png)



## Getting started

1) Download the Python sources.
2) Execute the use-case provided in [models/usecase.py](models/usecase.py)

If you want to explore the database and retrieve the results of this [article](https://doi.org/10.2514/6.2024-1707), follow these steps:

3) Download the CADO Airplane Database ([available upon request](#Contact)) and place it in the `database/` repository.
4) Run [generic_airplane_model.py](models/generic_airplane_model.py) with a Python interpreter (version 3.8 or higher)

You will find other data plot examples in [utils/data_analysis.py](utils/data_analysis.py). 

## Dependencies
This project relies on the following Python packages:
- Numpy
- Scipy
- Copy
- Pandas

## Funding

This research was funded by the Fédération ENAC ISAE-SUPAERO ONERA, Université de Toulouse, France.

## License

> This **second version** of GAM is available under the [GNU Lesser General Public License](LICENSE.txt).
> If you publish your work, please cite the following contribution in your work:
[Kambiri et al., *Energy consumption of Aircraft with new propulsion systems
and storage media*, Scitech Forum, Orlando, January 2024](https://doi.org/10.2514/6.2024-1707)
> 
> The *CADO airplane database* is available at [entrepot.recherche.data.gouv.fr](https://doi.org/10.57745/LLRJO0) under the [Open Database License](database/DATABASE_LICENSE.txt). Any rights in individual contents of the database are licensed under the [Database Contents License](http://opendatacommons.org/licenses/dbcl/1.0/).
> 
> In summary, you have the following freedoms:
> * **Freedom to Share**: You can copy, distribute, and use the database.
> * **Freedom to Create**: You can produce works based on the database.
> * **Freedom to Adapt**: You can modify, transform, and build upon the database.
> 
> However, you must adhere to these conditions:
> 
> * **Attribute**: You must attribute any public use of the database, or works produced from the database, in the manner specified in the Open Database License (ODbL). For any use or redistribution of the database, or works produced from it, you must make clear to others the license of the database and keep intact any notices on the original database. 
> * **Share-Alike**: If you publicly use any adapted version of this database, or works produced from an adapted database, you must also offer that adapted database under the ODbL. 
> * **Keep open**: If you redistribute the database, or an adapted version of it, then you may use technological measures that restrict the work (such as DRM) as long as you also redistribute a version without such measures.
> 
> This database was developed by the Conceptual Air transport Design and Operations (CADO) team at the French National School of Civil Aviation (ENAC) in Toulouse, France.


## Contact

For any request, please contact <yri-amandine.kambiri@enac.fr>